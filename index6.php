<?php
$dsn      = 'mysql:host=mysql301.phy.lolipop.lan;dbname=LAA1581428-pagetitle;charset=utf8mb4';
$user     = 'LAA1581428';
$password = 'pHKMBhz9TFpm';

// DBへ接続
try{
    $dbh = new PDO($dsn, $user, $password);

    // クエリの実行
    $query = "SELECT * FROM pagetitle where title_id BETWEEN 101 AND 120 ORDER BY post_date ASC limit 20";
    $stmt = $dbh->query($query);
?>



<!DOCTYPE html>
<html lang="ja" >

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>記事一覧p.6</title>
        <link rel="stylesheet" type="text/css" href="css/contents.css">
        <link rel="stylesheet" type="text/css" href="css/tag_align.css">
        <link rel="stylesheet" type="text/css" href="css/hanbarger.css">
        <link rel="stylesheet" type="text/css" href="css/bg_color.css">
        <link rel="stylesheet" type="text/css" href="css/set_footer.css">
    </head>
    <body>
        <header class="header">
            <div class="navtext-container">
                <div class="navtext">J8-jarnal</div><br/>
            </div>
            <input type="checkbox" class="menu-btn" id="menu-btn">
            <label for="menu-btn" class="menu-icon"><span class="navicon"></span></label>
            <ul class="menu">
                <li class="top"><a href="https://odysee.com/@jjjjjjj:9?view=content">odysee</a></li>
                <li><a href="https://twitter.com/ICT83003969">x</a></li>
                <li><a href="">過去の投稿一覧</a></li>
                <li><a href="https://info-j8-jaranal.main.jp/about/index.html">メールアドレス</a></li>
            </ul>

        </header>

        <!-- ページの一番上に置くアンカー -->
        <a id="top"></a>
        
        <br /><br /><br /><br />
        <h2 class="center">過去の投稿一覧リンク</h2>

        <div class="center"> 
            <div class="title_link">
                <?php //表示処理
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){?>
                        <a href="<?php echo $row["url"];?>"> 
                        <br/>
                        <b class="h2_title2"><?php echo $row["title"];?></a></b>&ensp;&ensp;<?php
                        echo $row["post_date"];?>
                        <br/>
                        <?php
                    }?>
            </div>
        </div> 
        <br /><br />

      
        <!-- <iframe class="news"
            src="https://nitter.1d4.us/nikkei/status/1747729314500800602#m"
            frameborder="0" allowfullscreen></iframe>
        <br /> -->

       
        <div class="right">
            <!-- トップに戻るボタン -->
            <button type="button" class="top_move" onclick="location.href='#top'"><img src="pic/arrow_top.png"></button>
        </div>

        <div class="post_index_bt_box">
                <button type="button" class="page" onclick="location.href='index.php'"><b class="page">1</b></button>
                <button type="button" class="page" onclick="location.href='index2.php'"><b class="page">2</b></button>
                <button type="button" class="page" onclick="location.href='index3.php'"><b class="page">3</b></button>
                <button type="button" class="page" onclick="location.href='index4.php'"><b class="page">4</b></button>
                <button type="button" class="page" onclick="location.href='index5.php'"><b class="page">5</b></button>
        </div>
            <br />

        <hr />
        <footer>
            <div class="footer_bg">
                <div class="footer_center"> 
                    6ページ<br /><br />
                    <button type="button" class="index_odysee" onclick="location.href='https://odysee.com/@jjjjjjj:9?view=content'">odysee</button>&ensp;
                    <button type="button" class="index_x" onclick="location.href='https://twitter.com/ICT83003969'">x</button>&ensp;
                    <button type="button"  class="index_postrecord" onclick="location.href='https://info-j8-jaranal.main.jp/index.html'">メールアドレス</button>
                    <br/><br/>
                </div>
            </div>    
        </footer>


    </body>
</html>

<?php
}catch(PDOException $e){
    print("データベースの接続に失敗しました".$e->getMessage());
    die();
}

// 接続を閉じる
$dbh = null;
?>
